# Include the file
- `from sqllib import sqlfetch`
- `from login import logindata`
- dont forget to include the logindata from the file _login.py_ and input the credentials there
# How to use it
## 1. Create object
- various arguments are possible, **logindata** is mandatory to supply

`connection_object = sqlfetch.SQLConnection(logindata=logindata)`
### To debug
- set `verbose` to True, if you want to see feedback on what the programm is doing
- set `show_traceback=True` to get the exception messages from python
### To download one value every 30 minutes
- add `dwnld_ival = 30` to the creation of the object
- for every second you can do `dwnld_ival = 1/60`
## 2. Declare variables
- **TIME_START** and **TIME_END** as
    - string: `'d-m-y H:M:S'` or
    - int epoch_timestamp: `1614581996500`
- **CAMERA**
    - from `0-12` with `0` being all cameras
## 3. Call the function
`connection_object.fetch_data_and_write_to_csv(ut_st=TIME_START, ut_end=TIME_END, camera=CAMERA)`

# Modes
# include boxes in download
- `connection_object = sqlfetch.SQLConnection(logindata=logindata, dwnld_box=True, conv_box=False)`
- to convert the boxes to midpoints (from [p1x, p1y, p2x, p2y, prob] to [p1x, p1y, prob]) set `conv_box=True`

# original download (no conversion of times and boxes)
- `connection_object = sqlfetch.SQLConnection(logindata=logindata, conv_t=False, dwnld_box=True)`
- then call the function

# Test mode
- this is a mode to check if data is available in the database
- to activate, add `test_mode=True` to the object creation and then call it in a loop execution

# Code examples
## imports and creation of object
    from sqllib import sqlfetch
    from login import logindata
    from datetime import datetime as dt
    conobj = sqlfetch.SQLConnection(logindata=logindata)

## simple execution

    def single_execution():
        TIME_START = '09-05-21 07:00:00'
        TIME_END = '09-05-21 07:00:01'
        CAMERA = 1
        conobj.fetch_data_and_write_to_csv(ut_st=TIME_START,
                                            ut_end=TIME_END,
                                            camera=CAMERA)

- to download just a day or just a few seconds, this approach can be used


## Loop exection

    def loop_ex():
        CAMERA = 1
        for i in range(1,30):
            TIME_START = f'{i}-05-21 06:00:00'
            TIME_END = f'{i}-05-21 20:20:00'
            conobj.fetch_data_and_write_to_csv(ut_st=TIME_START,
                                                ut_end=TIME_END,
                                                camera=CAMERA)

- to iterate over multiple days or months, this approach can be used


## check the connection
    def check_connection():
        conobj.establish_connection()

- will print and return "True" when connection is established
