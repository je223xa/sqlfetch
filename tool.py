# read the README please
from sqllib import sqlfetch
from login_full import logindata
conobj = sqlfetch.SQLConnection(logindata=logindata)

def loop_ex():
    CAMERA = 1
    for i in range(1, 30):
        TIME_START = f'{i}-05-21 06:00:00'
        TIME_END = f'{i}-05-21 06:00:01'
        conobj.fetch_data_and_write_to_csv(ut_st=TIME_START,
                                           ut_end=TIME_END,
                                           camera=CAMERA)


def single_execution():
    TIME_START = '02-05-21 13:00:00'
    TIME_END = '02-05-21 13:00:10'
    CAMERA = 1
    conobj.fetch_data_and_write_to_csv(ut_st=TIME_START,
                                        ut_end=TIME_END,
                                        camera=CAMERA)
def check_connection():
    print(conobj.establish_connection())


single_execution()