# 28.05
- added another parameter to the opt download range, that it doesnt try to do it on the first query
- but the threadcount is still in there, so it doesnt really make much sense

# 25.05
- changed the filename string
- changed the show progress string
- bugfix: changed the remove file to include a waiting time for windows maschines

# 22.05
- new file: query, for its own class
- there was an error in the time conversion, fixed that too
- added the removed _SECBEFTO variable, dunno why i deleted it

# 19.05
- git ignore and some more changes
- in sqlfetch  changed the thing when the array is 0

# 22.04
- fixed issue of d_st time, it would take the start_start time and not the temp_start time for the database
- is now fixed, it was a < not a >

# 21.04
- changed the json function again

# 18.04
- changed the error loop in the query function: once and the time decrement are now part of the query object
- error string includes time now
- time of wait increased to 0.5 seconds per wait

# 16.04
- changed the function with the json array, sometimes the array has no probability, so i set a rule with no probability

# 13.04
- removed download_ival from the boxconvert function, i have no idea why it was in there

# 11.04
- changed the readme
- changed the optimize function from 2 to 1.4 increase

# 07.04
- fix error on box conversion

# 06.04
- changed the optimisation of the download range to include bigger timeranges and when the last frame was empty
- added a test.py to plot the amounts of animals in the camera and to compare all datapoints to one every second

# 03.04
- well, i did a small bubu
- the header is just written on the first inc and so it doesnt turn up in the csv if the first chunk is empty
- therefore theres another variable now, that will fix the issue

- forgot one thisg, fixed in second commit

# 23.03
- changed the error handling of the server, i cant get rid of the print message so that stays
- changed max_time but reverted
- added a message if its too big, that was missing
- added the filepath to the delete command, that way you know what has been deleted

# 16.03
- changed the thread.isalive to conntrue
- added outofrange variable
- changed the shifting to the next day automatically
- THREAD WILL ALWAYS BE STARTED, CHECK NOT YET DONE!!
- works i think?, server still down
- added simple.py

# 14,03
- added a message when data has failed
- tried to download 100 MB of data - WORKS
- might keep it that way!

# 11.03
- do i want to redownload data when it failed??

# 03.03
- shifted some variables back and forth, deleted some
- im mostly trying to clean up the code now, get rid of all the vars that are at the beginning of the main

# 02.03
- i changed the day increment on overflow to use with mod of the day of the month
- this way it should also work in between months
- before: 31.05 aws smaller than 01.06
- no: 31 mod 31 is 0 and 1 31 is 1, it it would detect that we went one day ahead
- yep, tested. works like a charm
- shifted some files around and deleted some

# 01.03
- yey, its object oriented!
- i finally fixed all the issues, also had some problems with converting times and stuff
- but i just dont convert at the beginning, just the whole dataframe after downloading
- i also added docstrings and removed loads of functions
- i now iterate the times, without creating timechunks arrays, that is way cleaner and a lot faster for
    large datasets
- all the variables are in the functions now, just the constants stay at the top

- also added a variable to test the connection

- now i had to fix the issue of the download interval, but that is mostly fixed now
- the only thing that is not perfect is the end, there i miss one chink
# 28.02
- deleted the loading bar class, made it a function in the sql connection class called show_progress

# 25.02
- changed the filepath function
- cleared the logs
- changes the loading bar to just next, no bar anymore. it not worth the hassle
    - BUT i have to get the numbers to increase AFTER the thread has finished
    - could do that with the incloadingbar
- changed the gettsend function, it will work a bit better now, with less variables
- i wanted to remove the array as a variable
- changed the query object to take more arguments, the thread ID
- changed the writing to the file, removed the try except from it
- also changed the argument to the function, the id is part of the query object
- created the write to file function, and changed the generation of the header, this way i dont have an object variable anymore
- changed the call to the lastline variable
- im thinking of exluding the lineswritten variable, if its just for download i could del that too and just use the
    lines written
- THIS CANT WORK
- i am incrementing the thread ID twice with the same function
- first IN the thread and when its done also OUTSIDE of it. there have to be 2 variables
- -> YEP it writes the stuff twice. but also, different columns?! i dont understand. but it demands more looking into
- -> also defo test just using 1 thread
- changed the test_mode_del_file_check at the end of the function
- renamed all variables and shortened them
- includind thread- thrd, download - dwnld, chunk - chk, range - rng, time - t, loading bar - ld_b, boxes - box, convert - conv
- loading bar still needs fixing!!

# 24.02
- some error at the download interval, is fixed now
- changed the logging to a class too
- changed the query to a class too, at least the creation of the columns and stuff
- now no more logging levels, just verbose, so feedback or not


# 22.01
- changed te timechunks_download_pieces function that the times will be added up differently
- means: when a new day starts, it will reset the hour, minute and seconds of the start time and use that as the orientation
- that way it actually starts every day at the same time
- moved most of the functions into the class, that way it looks way cleaner
- still working on the loading bar, which should be its own class
- loading bar works now, time might not be right but i cant focus on that rn


# 13.02
- removed the conversion time from the chunkrange decision
- my queries are fast enough now
- changed my stupid math thing
- changed the timeout thing, made it a function
    - removed the function, changed it to just wait before creating the threads but thats about it
    - also removed the waiting before the threads and just put it in the main while loop
    - that was a stupid idea, now it just doesnt go in the whileloop
    - so now it stays in there
- removed the total time at the end, that wasnt accurate anyways
- will now try to put the whole thing in a class

# 08.02
- changed everything. literally
- its a library now, you can include it differently
- i changed the conversion of timestamps and boxes, while using dataframes as types and not arrays
- no iteration processes anymore
- measured time between queries and at different points in the program
- changed thee min time to 2 sec and the max to 3 (basically back to where it was)
- added variable called with frames, that is not used because i redid the conv algorithm``

# 06.02
- the loading bar time doesnt work
- two new variables, convert times and boxes
- one less variable download all which is now gone and is just set by the time interval
--------------------------------------------
- loading bar time works again
--------------------------------------------
- changed the docstrings
--------------------------------------------
- do i want to raise specific errors? i dont know if thats a good idea. might make the code more readable but i dont really
    need to...
- the thing is, since this is a library, it would be nice to have exceptions that i created myself that can be raised on specific errors
--------------------------------------------
- alright so i changed the folder design and everything
- i removed the logindata auto import function and excluded the file
- i added a global variable for the traceback, its just easier that way (not good, not terrible)
- now you can import the whole library
- and i redid the readme file
- added a new exception when end time is smaller than start time
- changed the loading bar string

# 05.02
- change the inital query time to 2 seconds, to fight that chun_range gets doubled in the beginning
- also moved the part of doubling the query in the if loop, that way it doesnt freakt out right in the beninging
- and also changed, that if its too short, it will just divide by 2
- and added the replace time to it

# 04.02
- i changed the convert in the dataframe, its a list now, it still has quotes but i cant change that
- also changed the loading bar to a child object, its a bit nicer that way, no variables
- and i want to call the percentage in the logging==2
- also added a bit to the midpoints function, when theres no probability in the array, im adding it at the end
- and the file gets opened and closed everytime which is nice beacause now you can actually see it filling up
    and when the function has an error, it all stays (thats what i wanted in the first place, i dont know why i implemented this now and not way earlier)
- and the frames work now(i hope they do at least....)
- and i have a message when some of the object boxes are empty
- and the loading bar function is a bit better written with the strings
- could be that the download with just one chunk is still crooked, i have no idea whats going on though

# 01.02
- fixed my mistakes from yesterdat 2am
- i changed the three variables that are in charge of counting the threads and handling how many are active
- one variable just gets incremented before the thread starts and then decr when it ends
- and both while loops (the one in conThread and the one at the end of the main while) will check if this is either set or 0
- then i also dont stop threads that havent been started yet, which is a good thing (obviously)
- oh, and i think the problem that the pool is exhausted shuold be fixed now once and for all. The while loop has now the active_thread_count in there, which will auto change. (i guess i just hope that theres no errors when 2 threads write the same variable at the same time)

# 31.01 - 01.02
- i changed the conversion of the times for the file
- i just removed one of the conversions, the one in the write-function, it was not necessary
- the time conversion works well and always cuts the milliseconds out now, except when you do download_original of course
- and i changed the check for the timezone, i did it with strings before, now i just check if the system time matches
    the EU time. if it does, good enough for me
    = sadly, when the right timezone is setup on the pc, but the time is shifted by an hour, my check doesnt work anymore. but who tf does that anyways, right?

- i found another problem, with the starting and removing of the threads that is now fixed
- i changed the waiting for the threads to a while loop that checks if there are still threads in the array, the problem was that i removed the threads before they even started, my function was too quick. so i wait now until at least one of the threads is started, then i actually remove threads as soon as theyre stopped
- and the function from convert_time now takes a datetime argument, this is much cleaner and should actually fix any problems
    with day first or last or whatever

# 30.01
- more documentation,
- the loading bar is a thread now that runs in the background

# 27.01
- new timechunks function for the download_all = False

# 26.01
- i just changed the dayfirst = True thing in the timeconvert function and now it loads way more data than before
    (while also converting the even months correctly)
- also fixed something with the threads and the increment of the id, the waiting was in the loop df.empty, so sometimes it didnt work
- and a bit at the stopping of threads, that also didnt work sometimes

# 22.01
- ive made some critical changes again
- the threads are working now
    theres a timechunks limit, download all is 1e3, else its 1e6
- i have an id for every thread that is counted up
- problem: it still takes really long to convert the points
    = so i changed the midpoint function a bit, but i dont know if thats enough

- commented everything and cleaned up the code a bit
- added a check that will raise an error when download_all and camera is 0
- commented more, this is finalised version now


# 19.01 - 21.01
- well, it took some time, everything changed now
- the mysqlconnection has a connection pool now that is used to connection
- the fetch takes it and downloads them, while just getting a connection everytime it makes a query
- like that i can download multiple of these things at the same time without wasting time
- i cant do the whole timeframe yet because i changed the while to an if
- that should be the last step, writing to the file already works well

# 13.01
- changes to the timezone
- more comments
--------------------------------------------
- another change to timezone, removed it, i have to look what the problem is
- added display.py to plot points
--------------------------------------------
- can now display all animals at once with indexes
- index dont work tho since we dont know which animal == which one
- display.py has more changes, more variables
- display less hardcode
--------------------------------------------
- new file: plotting, with the function for plotting the coordinates
- file display.py is just for calling it (for now at least)
--------------------------------------------
- added a function to redo the object boxes as pixels to the sqlfetch file


# 18.12
- test mode to be able to delete files even when data was found, can be used with tool.py > test.txt
- logging level now in arguments for main

# 16.12
- now also be able to add timestampserver to the function

# 15.12
- added language support
- redid the debug and info logic, now 4 levels 100 - 400 that determine the message
- languages are english and german so far

- changed the conversion from pandas from toint -> tofloat
- implemented check for timezone
- new variable: lines written
- files get deleted when lines_written == 0
- changed filename to include the date

# 14.12
- loadingbar percentage,
- timestamp to input are now dates,
- new function convert to convert the dates from UTC to GMT,
- download_all button to download either all or a range of values from the db,
- new class for active query: holds the current result (can be accessed with active_connection.active_query.query),
- no more chunksize, will now be calculated by the program depending on how big the interval is

- tried to change the code to 2 databases, but reverted most of it.
- left the wantboxes, which is now to put the boxes in the query. no more custom column assigning

# 13.12
- errormanagement, added dictionary for errormessages
- also put timechunks function private because i put it in the webapp and realised it was better that way, automatically by main

# 11.12
- now supports objects for every query, cleaner code, more options
- added more functions to the auto chunkrange

- added error variable to end the function on failure,
- added functionality to just download one camera from the timerange
- variable and class name changes
- small changes, readme and filenamecheck
- removed possibility to write to an array, just to the file


# 9.12
- sqlfetch has now direct write-to-file support, does not work perfectly yet;
- example.py has a way to make a custom query without using main function;
- sqlfetch.main will automatically increase the downloaded timechunks in one
- query, when the querytime is less than a certain timevalue. that way, we
- minimize the total downloadtime;
- also in sqlfetch.main there is a value to
- change the length of the loadingbar


# 27.11
- file for logindata and database name