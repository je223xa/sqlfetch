from datetime import datetime as dt
import pandas as pd
import threading
import ast
import os
import numpy as np


def run_once(fn):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return fn(*args, **kwargs)
    wrapper.has_run = False
    return wrapper

def threaded(fn):
    def wrapper(*args, **kwargs):
        thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
        thread.start()
        return thread
    return wrapper

def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def check_if_ts(t, max_ts: int = 1619000000000, t_format: str = '%d-%m-%y %H:%M:%S') -> dt:
    """check if the times that are put in are valid timestamps or dt strings"""
    if str(t).strip().isdigit():
        return dt.fromtimestamp(t/1000) if t >= max_ts else False
    else:
        t = dt.strptime(t, t_format)
        return t if t.year > 2020 and t.month > 2 else False


def json_array_to_midpoints_data_frame(lst_anml: str, width: int = 1920, height: int = 1080, frame: int = 0) -> pd.DataFrame:
    """convert a json array to a dataframe with midpoints"""
    lst_anml = ast.literal_eval(lst_anml)
    if len(lst_anml) > 0:
        f_len = True if len(lst_anml[0]) == 5 else False
        columns = ['p1x', 'p1y', 'p2x', 'p2y']
        if f_len:
            columns.append('prob')
        df_anml = pd.DataFrame(lst_anml, columns=columns)
        df_anml['frame'] = frame
        df_anml['p1x'] *= width
        df_anml['p2x'] *= width
        df_anml['p1y'] *= height
        df_anml['p2y'] *= height
        df_anml['px'] = np.round((df_anml['p2x'] - df_anml['p1x']) / 2 + df_anml['p1x'])
        df_anml['py'] = np.round((df_anml['p2y'] - df_anml['p1y']) / 2 + df_anml['p1y'])
        df_anml['prob'] = np.round(df_anml['prob'], decimals=3)
        return df_anml[['px', 'py', 'prob']] if f_len else df_anml[['px', 'py']]
    else:
        return pd.DataFrame([])
