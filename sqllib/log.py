class Log:
        """Language strings, currently two languages supported
        """
        germ = {
        110: "Auf Query thread warten.",
        111: "Datenbankabfrage:",
        113: "Array mit allen timestamps:\n",

        204: "Anzahl der Anfragen:",
        205: "csv Datei wird erstellt, name generiert.",
        206: "Der download wird für alle Elemente durchgeführt.",
        207: "Der Download wird nur für die Zeitabschnitte durchgeführt.",
        211: "Main: Auf Ende der Threads warten.",
        216: "Zeitintervall zu klein. Threadanzahl:  ",
        219: "Gesamtdownload erfolgreich abgeschlossen.",


        300: "Verbindung nicht gefunden. Versuche verbleibend: ",
        301: "Fehler bei ausführung aufgetreten.",
        303: "Zeitstempel umgedreht, wird automatisch geändert",


        410: "Fehler beim Schreiben in die Datei.",
        411: "Verbindungsthread ist nicht aktiv.",
        414: "CSV Dateiname nicht gültig.",
        415: "Keine Verbindung hergestellt und Error.",
        416: "So viele Threads auf einmal können nicht gestartet werden. Bitte unter 10 lassen.",
        417: "Der Computer befindet sich in der falschen Zeitzone. Bitte stellen sie ihn auf Westeuropa. Aktuell eingestellt: ",
        418: "Datei ist leer, es wurden keine daten heruntergeladen, Datei wurde gelöscht. Dateiname: ",
        419: "Kann nicht zu einem Timestamp konvertiert werden. Das Datum muss in Millisekunden angegeben werden und später als Montag, 1 März 2021 00:00:00 [1619000000000] sein.",
        420: "Kann nicht zu einem Datum konvertiert werden.",
        421: "Abbruch, da keine Verbindung zur Datenbank hergestellt werden konnte.",
        422: "Bitte nur eine Kamera auswählen.",
        424: "0 ist eine fehlerhafte Eingabe.",
        426: "Programmfehler bei den Zeitstempeln.",
        427: "Fehler beim erstellen der Datei.",
        428: "Die Liste mit den Logindaten ist fehlerhaft",
        429: "Die Startzeit ist grösser als die Endzeit.",
        430: "Verbindungsfehler",
        431: "Bitte einen Intervall > 1 sekunde wählen",

        500: "Abbruch durch Fehlermeldung",
        501: "Achtung: Manche heruntergeladenen Reihen enthalten keine Daten",
        502: "Fertig. Erstellte Datei:",

        600: "Zeit",
        601: "Wartend",
        602: "Aktiv",
        603: "Intervall",
        604: "Verbleibend",
        }


        engl = {
        110: "Waiting for query thread",
        111: "Database query:",
        113: "Array with all timestamps:\n",


        204: "Amount of queries:",
        205: "csv file is being generated, name not custom",
        206: "Downlad will be done for all rows",
        207: "Download will be done in intervals",
        211: "Main: Waiting for end of threads",
        216: "Timeframe too small, threadcount: ",
        219: "Download successful",

        300: "Connection not found. Tries left: ",
        301: "Error at execution",
        303: "Reversed timestamps, will be flipped",

        410: "Error at writing in the file",
        411: "Threat for connection is not alive",
        414: "CSV filename not valid",
        415: "No connection established and error raised",
        416: "You cant start that many threads, keep it under 6",
        417: "Your clock is set to the wrong timezone. Please change your Timezone to Western Europe. Currently set: ' ",
        418: "File is empty, no data was downloaded and file was deleted. Filename: ",
        419: "Cant convert this to a timestamp. The Timestamp has to be in milliseconds and no earlier than Monday, 1 March 2021 00:00:00 [1619000000000].",
        420: "Cant convert this to a date",
        421: "Abort due to no connection avaiable to database",
        422: "Please choose just one camera",
        424: "Some of the variables are missing to execute the function. Please makemsure everything is inputed correctly",
        426: "Some error at the timestamps",
        427: "Creating file error",
        428: "The logindata list is not correct",
        429: "start time is bigger then end time",
        430: "Connection error",
        431: "Please use an interval > 1 second",

        500: "Cancelled because of an error",
        501: "Disclaimer: Some rows turned out empty from the database",
        502: "Done. Created File:",

        600: "Time",
        601: "Waiting",
        602: "Aktive",
        603: "Range",
        604: "Remaining",
        }

        debug: bool = False

        def __init__(self, id, debug):
                if not id:
                        self.lang = self.engl
                else:
                        self.lang = self.germ
                self.debug = debug
