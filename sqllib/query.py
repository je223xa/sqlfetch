from datetime import datetime as dt

class Query:
    """Class for every Query, is supposed to hold start and end time, as well as SQL String and times"""
    _SECRNG: int = 7  # the range of seconds that will be downloaded for a specific time
    _ONCE = False
    _SECBEFTO = 500

    def __init__(self, boxes: bool, dwnl_ival: int, dt_start: dt, dt_end: dt, file_path: str, cur_rng: int, id: int, camera: int = 0, table: str = 'detections', debug: bool = False):
        """Init function"""
        self.dt_start: dt = dt_start  # start variabledatetime
        self.dt_end: dt = dt_end
        self.dwnld_ival: int = dwnl_ival
        self.camera = camera
        self.table = table
        self.file_path = file_path
        self.boxes = boxes
        self.cur_rng = cur_rng
        self.id = id
        self.debug = debug

    @property
    def query_string(self):
        """Generate the string from the object attributes"""
        ts_start = self.dt_start.timestamp() * 1000
        ts_end = self.dt_end.timestamp() * 1000
        if not self.dwnld_ival:
            query_string = f'SELECT {self.s_columns} FROM `{self.table}` WHERE `timestamp_server` >= {ts_start} AND `timestamp_server`< {ts_end}{self.with_camera}'
        else:
            query_string = f'SELECT {self.s_columns} FROM `{self.table}` WHERE `timestamp_server` > {ts_start - (self._SECRNG * 500)} AND `timestamp_server` < {ts_start + (self._SECRNG * 500)}{self.with_camera}'
        if self.debug:
            print(f'==Query {self.id}:: {query_string}==')
        return query_string

    @property
    def with_camera(self):
        """Append to the string if a camera is selected"""
        return f' AND `camera` = {self.camera}' if self.camera != 0 else ''

    @property
    def s_columns(self):
        """Get the columns for the downloads"""
        if self.boxes:
            return '`frameId`,`camera`, `objectCount`, `objectBoxes`, `timestamp_server`, `timestamp`'
        else:
            return '`frameId`,`camera`,`objectCount`, `timestamp_server`, `timestamp`'

    @property
    def l_columns(self):
        """Get the columns for the downloads"""
        if self.boxes:
            return ['frameId', 'camera', 'objectCount', 'objectBoxes', 'timestamp_server', 'timestamp']
        else:
            return ['frameId', 'camera', 'objectCount', 'timestamp_server', 'timestamp']
