from dateutil.relativedelta import relativedelta
from mysql.connector import pooling, errors
from sshtunnel import SSHTunnelForwarder
from datetime import datetime as dt, timedelta
from .log import Log
from .util import *
from .query import Query
import sshtunnel
import pandas as pd
import logging
import time
LOCALHOST = '127.0.0.1'


class SQLConnection:
    """Class for every new SQL connection, holds all important functions and everything thats important to run it"""
    # ==================== INT ====================
    # an error variable, to keep track of any errors that might happen
    _DWNLDTMIN: int = 15
    _DWNLDTMAX: int = 20
    # ==================== TIMES ====================
    __TTBRK: int = 10  # seconds until break from the connection funktion
    # ==================== OTHER ====================
    # the active query object, will be used for results etc (I could add the id here)
    # no more threads will be created cause the end of the timechks
    __TFORMAT: str = '%d-%m-%y %H:%M:%S'
    __LOGGER: logging = logging.getLogger(__name__)
    __SQLCOLS: list = ['camera', 'server', 'videoFilename', 'frameId', 'objectCount',
                       'objectBoxes', 'timestamp_server', 'timestamp']
    __PLCHD: str = ''.join(['    ' for i in range(10)])

    def __init__(self, logindata: list, dwnld_box: bool = False, thrd_c: int = 6, dwnld_ival: int = 0, disp_progress: bool = True,
                conv_box: bool = False, conv_t: bool = True, with_frames: bool = False, debug: bool = False, test_mode: bool = False, lang: int = 0) -> object:
        """Init function to create the object and check the data that was inputed
        Args:
            logindata (list): The login data as list [tunnel_url_or_ip_address, tunnel_username, tunnel_password, database_username, database_password, database_name, tunnel_port, table_name]
            dwnld_box (bool, optional): Download the objectBoxes column. Defaults to False.
            thrd_c (int, optional): The maximum amount of threads that can be started. Defaults to 6.
            dwnld_ival (int, optional): Download just intervals of times, not all data from the database. In minutes. Defaults to 0.
            disp_progress (bool, optional): Show the progress of the function. Defaults to True.
            conv_box (bool, optional): convert boxes from 2 points to 1 midpoint to save storage. Defaults to False.
            conv_t (bool, optional): Convert the times from UTC. Defaults to True.
            debug (bool, optional): Feedback for the user, for debug purposes. Defaults to False.
            lang (int, optional): Language. 0 for english, 1 for german. Defaults to 0.
            with_frames (bool, optional): _description_. Defaults to False.
            test_mode (bool, optional): _description_. Defaults to False.
        Returns:
            object: The object that is able to connect, write data to a file and more
        """
        # ==================== BOOL ====================
        self.conv_t: bool = conv_t
        self.dwnld_box: bool = dwnld_box  # want the objectBoxes
        self.disp_progress: bool = disp_progress
        self.conv_box: bool = conv_box
        self.with_frames: bool = with_frames
        # ==================== INT ====================
        self.thrd_c: int = thrd_c  # the number of threads that can be started
        self.dwnld_ival: int = dwnld_ival
        # how many threads can be started, and how many connections the tunnel
        self.logindata: list = logindata
        self.test_mode = test_mode
        self.lang = Log(lang, debug).lang
        self.dwnld_rng: float = 1 / 60 if dwnld_box else 6
        self.dwnld_rng = self.dwnld_rng if not self.dwnld_ival else 0.1
        self.error: int = 0
        self.debug = debug
        if not logindata or not (len(logindata) == 8):
            self.mes(428)
        if not self.thrd_c:
            self.mes(424)
        if not dwnld_ival:
            self.mes(206)
            pass
        if self.thrd_c > 30:
            self.mes(416)
        if self.conv_box and not self.dwnld_box:
            dwnld_box = True
        if self.dwnld_ival and self.dwnld_ival < 1/60:
            self.mes(431)

    def open_file(self, file_path='', append=True) -> None:
        """Open a file from the filepath"""
        if append:
            st = 'a'
        else:
            st = 'w'
        fpath = self.file_path if file_path == '' else file_path
        self.file_object = open(fpath, st, newline='')

    def close_file(self):
        """Close the file from the filepath"""
        if self.file_object:
            self.file_object.close
        self.file_object = None

    def opt_dwnld_rng(self, dwnld_rng, max_range) -> float:
        """Optimize the download range by taking the last query timedelta"""
        f = 1
        if not self.dwnld_ival and self.pool and self.act_thread_c > 1 and dwnld_rng < max_range and self.time_conv:
            if self.time_conv < timedelta(seconds=self._DWNLDTMIN) or self.last_dwnld_mty:
                f = 1.4
            elif self.time_conv > timedelta(seconds=self._DWNLDTMAX):
                f = 0.8
        return dwnld_rng * f

    def gen_file_path(self, camera, dt_start) -> bool:
        """Generate the filepath and set it to the object. Return True for generated, false for user input"""
        self.file_path = f'{self.folder_path}/d{dt_start.year}{dt_start.month}{dt_start.day}_t{dt_start.hour}{dt_start.minute}_c{camera}.csv'
        return True

    def write_to_file(self, data_frame=None, text=None, id=0) -> None:
        """Write dataframe to file from object"""
        # using no index in first column and drawing the header just at the first time
        if text:
            self.file_object.write(text)
            return True
        else:
            self.open_file()
            self.file_object.write(data_frame.to_csv(header=self.__WRTHDR, index=False))
            self.__WRTHDR = False
            # the file is closed and the data is submitted to the file
            self.close_file()
            return True

    def mes(self, num=0, add=''):
        """Return a Message"""
        if self.debug or num >= 300:
            if not num:
                print(f'{add}')
            else:
                print(f'{[num]}{self.lang[num]} {add}')
        if 400 <= num < 500:
            self.error = num

    def open_ssh_tunnel(self, ssh_host, ssh_username, ssh_password, debug=0):
        """Open the ssh tunnel with the details from the logindata"""
        if debug:
            sshtunnel.DEFAULT_LOGLEVEL = logging.DEBUG
        # the tunnel, including the maschine host as 2211, username and password
        try:
            self.tunnel = SSHTunnelForwarder(
                (ssh_host, self.logindata[6]),
                ssh_username=ssh_username,
                ssh_password=ssh_password,
                remote_bind_address=(LOCALHOST, 3306),
            )
        except:
            return False

    def create_pool(self, database_name, database_username,
                    database_password, pool_name="POOL"):
        """Create the connection pool"""
        try:
            self.pool = pooling.MySQLConnectionPool(
                use_pure=True,
                pool_name=pool_name,
                pool_size=self.thrd_c,
                port=self.tunnel.local_bind_port,
                host=LOCALHOST,
                user=database_username,
                database=database_name,
                password=database_password,
            )
        except:
            return False
        return True

    def run_query(self, sql):
        """Generate a cursor, run a query"""
        conn = self.pool.get_connection()
        result = pd.read_sql(sql, conn)
        conn.close()
        return result

    def read_query(self, query):
        conn = self.pool.get_connection()
        cursor = conn.cursor()
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            conn.close()
            return result
        except Exception as err:
            print(f"Error: '{err}'")

    def establish_connection(self) -> bool:
        """Generate a connection pool through a tunnel
        Args:
            logindata (list): list of logindata
            self (SQLConnection): SQL connection object
            self.__TTBRK (int, optional): how many seconds the function will try to reconnect to the database. Defaults to 10."""
        # open the tunnnel, use the logindata that has been provided
        try:
            self.open_ssh_tunnel(
                self.logindata[0], self.logindata[1], self.logindata[2])
            self.tunnel.start()
            # start the tunnel, if not, exit function
        except Exception:
            self.mes(430)
            return False
        try:
            self.create_pool(
                self.logindata[5], self.logindata[3], self.logindata[4])
            return True
        except Exception:
            self.tunnel.close
            self.mes(405)
            return False

    @threaded
    def connect(self) -> bool:
        """THREADED: Establish Tunnel and Database connection"""
        self.tunnel: SSHTunnelForwarder = None  # the tunnel
        self.pool: pooling.MySQLConnectionPool = None  # the connection pool
        if not self.establish_connection():
            return False
        if not self.error:
            self.conn_true = True
        else:
            return False
        # ========================================================================
        while not self.main_dne and not self.error:
            pass
            # when theres no connection, wait until timeout and raise an error
            if not self.pool or self.tunnel:
                while self.__TTBRK:
                    if self.pool and self.tunnel or self.error:
                        break
                    self.__TTBRK -= 1
                    time.sleep(1)
                if not self.__TTBRK:
                    self.mes(408)
        # when its all done, pool will be closed and tunnel will be closed. I make sure to check if both are open, because the function also goes in here when an error is raised
        if not self.error:
            self.disconnect()
        return True

    def disconnect(self):
        self.conn_true = False
        if self.pool:
            self.pool._remove_connections()
        if self.tunnel:
            self.tunnel.close()

    @threaded
    def show_prgress(self) -> bool:
        """THRADED: Show Loadingbar progress while execution"""
        while not self.error and not self.main_dne and self.disp_progress:
            try:
                progress = f'{self.lang[600]}: {self.dt_temp_end} {self.lang[601]}: {self.w_thrds}; {self.lang[602]}: {self.act_thread_c}; {self.lang[603]}: {round(self.dwnld_rng, 2)} min; {self.lang[604]}: {self.rem_thrds}' if self.conn_true else 'connecting...'
                if not self.debug:
                    print(f"\r| {progress} {self.__PLCHD}", end='\r')
                    pass
            except AttributeError:
                print(f"\rsetup..", end='\r')
            time.sleep(0.1)
        return False

    @threaded
    def query_and_output(self, cur_query) -> bool:
        """THREADED: Make a query, convert the data and write it to a file
        Args: cur_query (Query): A Thread object"""
        self.last_dwnld_mty = False
        if self.error:
            return False
        # get the time before the query started, just to keep track of everything
        self.time_bef_conv = dt.now()
        # generate the query string from the query object
        try:
            # data_frame = self.run_query(cur_query.query_string)
            data_frame = pd.DataFrame(self.read_query(
                cur_query.query_string),  columns=cur_query.l_columns)
        except Exception as e:
            self.mes(430, f' time: {cur_query.dt_start}')
            return False
        # just replace them when the dataframe is not empty
        if not data_frame.empty:
            if self.dwnld_ival:
                # to get the rows where it has 0 seconds, i have to
                # divide the timestamp_server by 1000 to cut the milliseconds
                # then do modulo 10
                # and compare it to 0, this way, i will always get the seconds with 0
                # and since i dont need all of them, I just take the first one and cut the others off
                divider = 100 if self.dwnld_ival and self.dwnld_ival < 1 else 1000
                data_frame = data_frame[(
                    (data_frame.timestamp_server / divider) % 10) < 5].head(1)
            if self.conv_t:
                data_frame.timestamp = pd.to_datetime(data_frame.timestamp_server / 1000, unit='s').\
                    dt.tz_localize('Etc/GMT').\
                    dt.tz_convert('Europe/Berlin').\
                    dt.strftime(self.__TFORMAT)
                pass
            if self.conv_box and not data_frame.objectBoxes.empty:
                   data_frame.objectBoxes = data_frame.apply(lambda row: json_array_to_midpoints_data_frame(
                        row['objectBoxes'], frame=cur_query.id).values.tolist(), axis=1)
        else:
            self.dwnl_incmplte = True
            self.last_dwnld_mty = True
        self.time_conv = dt.now() - self.time_bef_conv
        while self.dn_thrds < cur_query.id:
            if not cur_query._ONCE:
                self.w_thrds += 1
                cur_query._ONCE = True
            if self.error:
                print(f'Timeout at {cur_query.dt_start} {self.__PLCHD}')
                return False
            if cur_query._SECBEFTO:
                cur_query._SECBEFTO -= 1
                time.sleep(0.5)
            else:
                self.mes(430)
        if self.w_thrds:
            self.w_thrds -= 1
        if not data_frame.empty:
            self.last_line += data_frame.size
            # its not really lines_written but more times written to file
            self.lines_written += 1
            # now the file gets opened again, for every query, while appending the current result
            self.write_to_file(data_frame=data_frame, id=cur_query.id)
        # increment the current thread id, to tell the next thread to dump its result into the file
        self.act_thread_c -= 1
        self.dn_thrds += 1
        # now i calculate the time, that the replacing took
        self.time_total_conv += self.time_conv
        return True

    def fetch_data_and_write_to_csv(self, ut_st, ut_end, camera: int = 0, clear: bool = False, folder_path: str = 'csv') -> bool:
        """Main function that calls all the subfunctions to start the connection and the threads while the time will be incremented in a loop cur_query._SECBEFTO its done
        Args:
            ut_st (int or string): the *unkown_start* time. can be string or int
            ut_end (int or string): the *unkown_end* time, can be string or int
            camera (int, optional): the selected camera to download. Defaults to 0.
            file_path (str, optional): the filepath for the file that will be written. Defaults to ''.
            clear (bool, optional): clear screen before execution. Defaults to False.
        """
        self.folder_path = folder_path
        self.main_dne: bool = False  # variable that successfully ends everything
        self.dwnl_incmplte: bool = False
        # set true when the connection is established
        self.connection = None  # the connection
        self.file_object = None  # the output file
        self.time_total_conv: timedelta = timedelta(
            seconds=0)  # the time that all queries took
        self.last_line: int = 0  # the last line that was written, needed for the dataframe
        self.lines_written: int = 0  # how many lines have been written to the csv file
        # the id that will be written to the thread, aka a count of
        self.thread_id: int = 0  # the id of the thread that is currently active
        # the amount of threads that are currently active, set by the threads themselves
        self.act_thread_c: int = 0
        # the threads that are done
        self.dn_thrds: int = 0
        # the threads that are waiting to write to file
        self.w_thrds: int = 0
        self.__WRTHDR = True  # this is true when the header is written

        # =================================== CHECK VARIABLES =====================================
        # =========================================================================================
        if clear:
            cls()
        if self.error:
            return False
        # if the dwnld all is not set and all the cameras should be dwnlded, raise an error. That has to be done individually
        if self.dwnld_ival and camera == 0:
            self.mes(422)
            return False
        # ============== CHECK TIMES ===============
        # check if its a timestamp or a string
        dt_start = check_if_ts(ut_st)
        dt_end = check_if_ts(ut_end)
        if not dt_start or not dt_end:
            self.mes(419)
            return False
        # this is to exchange the dates if someone messed up start and end
        if dt_end < dt_start:
            self.mes(303)
            temp = dt_start
            dt_start = dt_end
            dt_end = temp
        # ============== PROGRESS BAR ==============
        # start the progress bar, to give feedback to the user
        self.show_prgress()
        # =================================== SET VARIABLES =======================================
        # =========================================================================================
        # the maximum range of chks that can be dwnlded at once
        max_range: int = 10 if self.dwnld_box else 100
        # the interval in seconds, is needed for the dwnld_ival = True, to get a few of the timechks +- the chosen one
        # a debug string that can be printed to inform the user of his choices
        debug_string: str = f'| DB {self.logindata[5]}| Tabelle {self.logindata[7]}| Kamera {camera}| Monat {dt_start.month}| Tag {dt_start.day}'
        # =================================== FILE PATH ===========================================
        # =========================================================================================
        # if the filename was not set by the function, it will be set here
        # by the dt object that was created earlier
        if self.gen_file_path(camera=camera, dt_start=dt_start):
            self.mes(205)
        # here the file just gets opened once and then immediately closed. im doing this to check if the file is valid and
        # if it makes sense to import it
        try:
            self.open_file(append=False)
        # if this is raised, the directory might not be initialised
        except FileNotFoundError:
            os.mkdir(self.folder_path)
            try:
                self.open_file(append=False)
            except Exception:
                self.mes(410)
                return False
        except Exception:
            self.mes(410)
            return False
        finally:
            self.close_file()
        # thread will be started that takes the connection pool over the tunnel, it will keep running until the function is done
        # =================================== CONNECTION THREAD ===================================
        # =========================================================================================
        # try to start the first thread to establish the connection
        self.conn_true:bool = False
        con_thread = self.connect()
        dt_temp_st = dt_temp_end = dt_temp_d_st = dt_start
        while not self.conn_true and not self.error:
            pass
        # =================================== MAIN LOOP ===========================================
        # =========================================================================================
        while dt_temp_end < dt_end and not self.error and self.conn_true:
            no_more_threads = True if self.error else False
            while not self.pool and not self.error:
                pass
            # =================================== QUERY THREADS ===================================
            # =====================================================================================
            # This is the main loop that will start the function. It will go in here when:
            # - the given threadcount is not reached yet
            # - the current time is below the max, so the function is scur_query._SECBEFTO working
            # - no error is raised
            # - the connection thread is scur_query._SECBEFTO alive
            # - and we are not at the end of the threads yet (i added this as a second security to make sure we dont go over the level of all chks)
            if self.act_thread_c < self.thrd_c and dt_temp_end < dt_end and not self.error and self.conn_true and not no_more_threads:
                # ================= REMAINING =================
                self.rem_thrds = (dt_end - dt_temp_end).total_seconds() / (self.dwnld_rng * 60) / 10
                self.rem_thrds /= self.dwnld_ival if self.dwnld_ival else 1
                self.rem_thrds = int(self.rem_thrds)
                # ================= TIME INC =================
                # get the time for this current iteration, used for the progress
                self.dt_temp_end = dt_temp_end
                # the old end time for the query will be the new start time for it
                self.dt_temp_st = dt_temp_st = dt_temp_end
                # if the start is in between the inactive hours, the start will be pushed forwards to the next day and then shifted to hour 5 of that next day
                st_out_of_rng = True if (5 > dt_temp_st.hour > 22) else False
                if st_out_of_rng:
                    day_inc = 1 if dt_temp_st.hour > 0 else 2
                    dt_temp_st += relativedelta(hour=5, day=day_inc)
                # if there is an interval set,
                if self.dwnld_ival:
                    # i get the time before and after the increment
                    d_bef = dt_temp_end  # the current day
                    dt_temp_end += timedelta(minutes=self.dwnld_ival)
                    d_aft = dt_temp_end  # the day after the increment
                    # and then calculate the difference between the two days
                    # i use the modulo operator that, when it goes over 2 months, it scur_query._SECBEFTO counts them as added upwards. otherwise 31 > 1 . Now 31%31=0 < 1%31
                    d_rng = d_aft.day % (d_aft + relativedelta(day=31)).day \
                    - d_bef.day % (d_bef + relativedelta(day=31)).day
                    # if thats bigger than 0, i just simply add the days to a variable that i set before the loop
                    # this could also be the start time, but i wanted a new one
                    if d_rng > 0:
                        dt_temp_d_st += timedelta(days=d_rng)
                        # and then, because i want to compare the temporary end to the bitter end,
                        # i will set end = start
                        # it doesnt matter, because i just use the start in my query
                        dt_temp_end = dt_temp_d_st
                else:
                    # if not, just increment by the range that i set and dynamically change
                    dt_temp_end += timedelta(hours=self.dwnld_rng)
                # ================= THREAD END =================
                # if were at the end, we set the temp end to it
                # if an interval is set, also start is put to this value. but that doesnt matter anyways, cause i will never
                # do it a last time. but thats good, cause i dont want to have a random value at the end. So i leave it like that
                if dt_temp_end >= dt_end:
                    dt_temp_end = dt_end
                    if self.dwnld_ival:
                        dt_temp_d_st = dt_end
                    no_more_threads = True
                # ========= OPTIMIZE RANGE =========
                # this has been added cause on windows mashines there might be a problem with that variable
                # if self.time_conv:
                self.dwnld_rng = self.opt_dwnld_rng(self.dwnld_rng, max_range)
                # =================================== QUERY OBJECT ================================
                # =================================================================================
                # the query object will be created
                cur_query = Query(dwnl_ival=self.dwnld_ival,
                                  boxes=self.dwnld_box,
                                  dt_start=dt_temp_st,
                                  dt_end=dt_temp_end,
                                  file_path=self.file_path,
                                  camera=camera,
                                  cur_rng=self.dwnld_rng,
                                  id=self.thread_id,
                                  table=self.logindata[7],
                                  debug=self.debug)

                # ================= QUERY STRING =================
                # get the query string, this is just for debug and has nothing to do with the actual query,
                # the string will be generated new for every query
                self.mes(111, cur_query.query_string)
                # ================= QUERY THREAD =================
                # the thread for the query will be generated
                self.act_thread_c += 1
                self.query_and_output(cur_query)
                # increment the threadID
                self.thread_id += 1

        # =================================== WAIT FOR REMAINING THREADS ==========================
        # =========================================================================================
        # and i do this by just simply waiting until there are no active threads anymore
        # i decrement the value every time a query stops, so its easy
        # at the end of the while loop, it will wait until all the active current threads are ended
        while self.act_thread_c and not self.error:
            pass
        # =================================== PRINT RESULT ========================================
        # =========================================================================================
        if self.act_thread_c > 1:
            if self.lines_written and not self.test_mode and not self.error:
                if not self.file_object:
                    self.open_file()
                print(f"\n{self.__PLCHD}")
                self.mes(502, self.file_path)
                if self.dwnl_incmplte:
                    self.mes(501)
                    pass
        if os.path.exists(self.file_path):
            if not self.lines_written and not self.test_mode:
                self.mes(418, self.file_path + self.__PLCHD)
                os.remove(self.file_path)
                time.sleep(0.2)
            if self.test_mode and not self.error:
                if self.lines_written:
                    self.open_file(file_path='csv/output.txt', append=True)
                    outstr = f'JJ{debug_string}'
                    print(f'{outstr}{self.__PLCHD}')
                    self.write_to_file(text=f'{outstr}\n')
                    self.close_file()
                elif not self.lines_written:
                    self.open_file(file_path='csv/output.txt', append=True)
                    outstr = f'NN{debug_string}'
                    print(f'{outstr}{self.__PLCHD}')
                    self.write_to_file(text=f'{outstr}\n')
                    self.close_file()
                if self.file_object:
                    self.close_file()
                os.remove(self.file_path)
                time.sleep(0.2)
        if self.file_object:
            self.close_file()
        # =================================== PRINT ERROR =========================================
        # =========================================================================================
        # if there was an error on execution and thats the reason why the while loop ended,
        # print the message and close connection and tunnel, if open
        if self.error:
            return False
        # =================================== CLEAN UP ============================================
        # =========================================================================================
        # we can tell everyone that all the queries are done and that we can end all the functions
        self.main_dne = True
        # the main function will wait until the connection thread has cleaned up
        con_thread.join()
        # at this point, the connection is reset and the tunnel is closed!
        if self.error:
            self.disconnect()
            return False
        self.mes(219)
        return True
